# Fast k-end
El proyecto busca ayudar a acelerar en el desarrollo del backend de una página web(escrito en java), creando módulos (servicios) básicos de una web como: gestión de usuarios y permisos, facturación y registro, envío automático de emails, gestión de inventario, compras, entre otros, usuarios, sistemas de pago, etc.

## Tabla de contenido

- [Modulos](#modulos)
  - [File System](#file-system)
  - [Persistence System](#persistence-system)
  - [Pay System](#pay-system)
  - [POO Patterns](#poo-Patterns)
  - [User](#user)
- [Empezar](#empezar)
  - [Prerequisitos](#prerequisitos)
  - [Instalacion](#instalacion)
  - [Manual de usuario](#manual-de-usuario)
- [Licencia](#licencia)

## Modulos
Un conjunto de modulos que conforman un marco de trabajo para el desarrollo de backeds

### File system
Este sistema gestiona el manejo de archivos.

### Persistence System
Este paquete se ecarga de obtener informacion especifica de la persistencia, ya sea:
 * Un archivo
 * Una base de datos, 
 Si esta es remota se aplica el patron proxy, quedando una clase real en el servidor y una proxy en el cliente.

### Pay System
Este es un sistema que por el momento solo simula un pago con una base de archivos, se probee un paquete API donde esta todo lo que se necesita para usar este modulo.

### POO Patterns
Este modulo contiene un conjunto de patrones poo en su forma abstracta.

Los Patrones actuales son:
  * Observer
  * MVC Controllers

### User
Este modulo tiene clases que modelan usuarios de un sistema de la forma mas basica.
No maneja permisos ni nada por el estilo.
Solo modelan la informacion de un usuario.

## Empezar
### Prerequisitos
Java 8, un IDE como Eclipce o Netbeans
### Instalacion
   1. Decargar el framework: Puede descargar el codigo funte o el binario JAR
      - Clonar el repositorio: Si esta usando eclipce o netbeans, busque como colonar un repositiorio git.
      - Obtener el JAR: Descargar el archivo JAR [fast-k-end](./build/fast-k-end-modules.jar)
   2. Importar el framework en el proyecto donde lo vaya a usar.
### Manual de usuario
El manual de usuario fue generado con Doxigen, para leer el manual de usuario acceda al siguiente [link](./docs/user-manual.pdf)


# Licencia
Este programa es software libre: puede redistribuirlo y/o modificarlo según los términos de la Licencia Pública General de GNU Affero publicada por la Free Software Foundation, ya sea la versión 3 de la Licencia, o (a su elección) cualquier versión posterior. Consulte el archivo [LICENCIA](./LICENCIA.md) en nuestro repositorio para ver el texto completo.