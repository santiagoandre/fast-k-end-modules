package fast_k_end.user;

import fast_k_end.poo_patterns.observer.Observed;

import fast_k_end.poo_patterns.observer.Observer;
import fast_k_end.poo_patterns.observer.ObserverCode;



public class User extends AbsUser implements Observer{
	private Contact contact;
	private String name;
	private String lastname;
	/*un usuario puede tener mas cosas
	 *asi que dejo esta clase como abstrata
	 */
	/*Este es el cliente de la aplicacion, hay muchos y estos estaran loggeados
	 *en diferentes lugares(CLiente / Servidor).
	 *Esta clase reprecenta la persistencia de 
	 */
	public User(String name, String lastname,String username, String  password){
		super(username, password);
		this.name = name;
		this.lastname= lastname;
	}
	public void addContact(Contact contact){
		this.contact = contact;
	}
	public Contact getContact(){
		return this.contact;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	@Override
	public String toString() {
		return "User [contact=" + contact + ", name=" + name + ", lastname="
				+ lastname + "]";
	}
	@Override
	public void notify(Observed evento,ObserverCode code) {
		this.notify_observers(evento,code);//notificar a la sesion 
		
	}
	
}
