package fast_k_end.user;
/**
 * Reprecenta un contacto(email,telefono,etc)

 *
 */
public class Contact {
	private String email;
	private int phoneNumber;
	public Contact(String email, int phoneNumber) {
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	public Contact(String email) {
		this.email = email;
	}
	public Contact(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
