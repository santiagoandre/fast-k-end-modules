package fast_k_end.user;

import fast_k_end.poo_patterns.observer.Observed;


/**
 * Representa la informacion de un usuario
 * 
 *
 */
public class AbsUser extends Observed{

	private String username;
	private String password;
	public AbsUser(String username, String  password){
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
