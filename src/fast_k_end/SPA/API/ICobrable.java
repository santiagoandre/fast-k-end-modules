package fast_k_end.SPA.API;
/**
 * Interfaz que define un metodo 'Cobrar'. 
 */
public interface ICobrable {
	/** Cobra una canticad, y cuando termina el proceso de cobro se le notifica al cliente 
	 *
	 */
	public void cobrar(int cantidad,IClienteSistemaPago cliente);
	/** Determina si el pago fue exitoso
	 * 
	 */
	public boolean isSuccess();

}
