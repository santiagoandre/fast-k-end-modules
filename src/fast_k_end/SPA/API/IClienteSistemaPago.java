package fast_k_end.SPA.API;

public interface IClienteSistemaPago {
	/**
	 * Interfaz que implementa un objeto que quiere ser notificado cuando un cobro esecifico termine.
	 */
	public  void notificar(ICobrable c);
}
