package fast_k_end.SPA.API;

public class Test  implements IClienteSistemaPago{

	public static void main(String[] args) {
		Test t = new Test();
		FabricaSPArchivo f = new FabricaSPArchivo();
		ICobrable sistema = f.getInstance();
		sistema.cobrar(1000, t);

	}

	@Override
	public void notificar(ICobrable c) {
		System.out.println("El estado del pago es: "+c.isSuccess());
	}

}
