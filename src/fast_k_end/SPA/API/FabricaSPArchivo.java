package fast_k_end.SPA.API;

import fast_k_end.SPA.controller.CerrarSistemaController;
import fast_k_end.SPA.controller.CredencialesPagoController;
import fast_k_end.SPA.controller.MetodosDePagoController;
import fast_k_end.SPA.model.AbsApp;
import fast_k_end.SPA.model.App;
import fast_k_end.SPA.view.FormCredencialesPago;
import fast_k_end.SPA.view.VMetodosDePago;
import fast_k_end.poo_patterns.observer.ObserverCode;
public class FabricaSPArchivo implements IFabricaSistemaPago{
	
	@Override
	public ICobrable getInstance() {
		return generarSistema();
	}
	private AbsApp generarSistema(){
		//creo el modelo
		AbsApp app = new App();
		//creo las vistas
		VMetodosDePago interfazSistema= new VMetodosDePago();
		FormCredencialesPago fromCredenciales = interfazSistema.getFormCredenciales();
		//creo los controllers y los asocio al modelo y sus vistas
		CredencialesPagoController pagocller = new CredencialesPagoController(fromCredenciales,app);
		MetodosDePagoController metodospagocller = new MetodosDePagoController(interfazSistema,app);
		CerrarSistemaController cerrarcller = new CerrarSistemaController(app,interfazSistema);
		//asocio la vista principal al modelo		
		app.addObserver(interfazSistema);
		//asocio los controllers con las vistas
		fromCredenciales.setController(pagocller);
		interfazSistema.setControllerCbx(metodospagocller);
		interfazSistema.setControllerWindow(cerrarcller);
		//llamo el metodo actualizar para la vista por defecto 
		interfazSistema.notify(app, ObserverCode.LAUNCH);
		metodospagocller.actualizar();
		return app;
	}
}
