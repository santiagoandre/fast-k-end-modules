package fast_k_end.SPA.model;

import java.util.HashMap;
import java.util.List;

public class ServicioPagoTarjetaDebito extends ServicioPagoArchivo {
	public ServicioPagoTarjetaDebito(){
		this.pathBase = "basePago/tarjetaDebito.txt";
	}


	@Override
	protected boolean validar(String metodo, List<String> credenciales) {
		//validar cada linea del archivo
		String[] datos = metodo.split(" ");
		//if(datos.length<3 ) //hay un error
		int codigo = Integer.parseInt(datos[0]);
		return Integer.parseInt((String) credenciales.get(0))== codigo;
	}

	
	@Override
	protected void procesarDatos(String metodo) {
		String[] datos = metodo.split(" ");
		datosMetodoPago = new HashMap<>();
		datosMetodoPago.put("codigo", datos[0]);
		datosMetodoPago.put("saldo", datos[1]);
	}
	@Override
	public HashMap<String, Class> getCredencialesNecesarias() {
		HashMap<String, Class> credenciales = new HashMap<>();
		credenciales.put("Codigo Desbloqueo", int.class);
		return credenciales;
	}


}
