package fast_k_end.SPA.model;

import java.util.HashMap;
import java.util.List;

import fast_k_end.poo_patterns.observer.ObserverCode;



public abstract class ServicioPago
{
	public static final int SUCCESS = 0; //exito se realizo el pago
	public static final int UNAUTHENTICATED = 1; //no se autentifico el usuario,
	public static final int INSUFFICIENT_BALANCE = 2; //saldo insuficiente
	public static final int ERROR = 3; //algun otro error
	protected abstract boolean validar(List<String> credenciales);//valida las credenciales del metodo de pago
	protected abstract boolean validarSaldo(int saldominimo);//valida si el saldo es suficiente
	protected abstract boolean actualizarSaldo(int cantidadCobrar); //cobra, sabiendo 
	protected ObserverCode cobrar(int cantidad,List<String> credenciales){
		try{
			if (!validar(credenciales))
				return ObserverCode.UNAUTHENTICATED;
			if(!validarSaldo(cantidad))
				return ObserverCode.INSUFFICIENT_BALANCE;
			if (actualizarSaldo(cantidad))
				return ObserverCode.PAYERROR;
		}catch(Exception e){
			return  ObserverCode.PAYERROR;
		}
		return ObserverCode.PAYSUCCESS;	
	}
	public abstract HashMap<String, Class> getCredencialesNecesarias();
	
	
}

