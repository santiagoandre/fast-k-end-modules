package fast_k_end.SPA.model;

import java.util.ArrayList;

import java.util.List;

public  class App extends AbsApp

{

	//se puede crear una clase que haga esto con refleccion o leyendolo de un archivo
	//cada qie se creen mas metodos de pago hay que aditar esta clase
	//haciendo refleccion no se rompe el principio de open closed
	@Override
	public List<String> getTiposMetodosDePago() {
		ArrayList<String> metodos = new ArrayList<>();

		metodos.add("Tarjeta de credito");
		metodos.add("Tarjeta debito");
		return metodos;
	}



	@Override
	public void crearServicio() {
		switch(this.metodoPago){
		case "Tarjeta de credito":	
			servicio = new ServicioPagoTarjetaCredito();
			break;
		case "Tarjeta debito":	
			servicio = new ServicioPagoTarjetaDebito();			
			break;
		}
		
	}

	

	

	
}

