package fast_k_end.SPA.model;


import java.util.HashMap;




import java.util.List;

import fast_k_end.file_system.File;




public abstract class ServicioPagoArchivo extends ServicioPago
{
	protected String pathBase; 	//atributo del archivo.
	private File base;
	protected HashMap<String,String> datosMetodoPago;
	private int posicion;

	
	@Override
	protected boolean validar(List<String> credenciales) {
		abrirArchivo();
		String sig = getSigMetodoPago();
		int contador =0;
		while(sig!= null && !sig.isEmpty()){//se recorren todos los metodos de pago reistrados en el archivo, liena a linea
			if(validar(sig,credenciales)){// si esa linea corresponde con las credenciales ha sido autentificado
				procesarDatos(sig);
				posicion = contador;
				return true;
			}			
			sig = getSigMetodoPago();
			contador++;
		}
		return false;//no existio ninguna coincidencia
	}
	@Override
	protected boolean validarSaldo(int saldominimo) {
		//obtener el saldo
		String saldo = datosMetodoPago.get("saldo");
		return saldominimo<= Integer.parseUnsignedInt(saldo);
	}




	@Override
	protected boolean actualizarSaldo(int cantidadCobrar) {
		realizarCobro(cantidadCobrar);// se actualiza el String de datos.
		//escribirlo en el archivo
		cambiarLinea(posicion,generarStringDatos());
		return false;
	}


	private String generarStringDatos() {
		String stringdatos = "";
		for(String dato: datosMetodoPago.values()){
			stringdatos = stringdatos+ " " + dato;
		}
		return stringdatos.substring(1,stringdatos.length());
	}


	
	//metodos de validacion y modificacion de cadena de datos
	protected abstract void  procesarDatos(String sig) ;
	
	protected abstract boolean validar(String metodo, List<String> credenciales) ;
	
	protected void realizarCobro(int cantidadCobrar) {
		String saldo = datosMetodoPago.get("saldo");//obtiene el saldo
		int nuevoSaldo = Integer.parseInt(saldo)-cantidadCobrar;//lo actualza
		datosMetodoPago.replace("saldo",Integer.toString(nuevoSaldo));  //lo cambia
	}
	//metodos de manipulacion de archivos
	private String getSigMetodoPago() {
     	return base.Leer();
	}
	private void cambiarLinea(int posicion, String datosMetodoPago) {
		base.cambiarLinea(posicion, datosMetodoPago);
	}
	public void abrirArchivo(){
		base  = new File();
		base.abrirArchivo(pathBase);
	}
	public void cerrarArchivo(){
		if(base != null)
			base.close();
	
	}
}

