package fast_k_end.SPA.controller;


import java.util.ArrayList;










import fast_k_end.SPA.model.App;
import fast_k_end.SPA.view.FormCredencialesPago;
import fast_k_end.poo_patterns.mvc_controllers.ButtonController;
import fast_k_end.poo_patterns.observer.Observed;

public class CredencialesPagoController extends ButtonController

{
	private FormCredencialesPago view;
	private App app;
	public CredencialesPagoController(FormCredencialesPago aView, Observed aModel) {
		super();
        view = aView;
        app = (App) aModel;

	}
	@Override
	public void actualizar() {
		ArrayList<String> credenciales = view.getCredenciales();
		app.cargarCredenciales(credenciales);
		app.iniciar();
	}

	@Override
	public boolean isValid() {
		return view.esValido();
		
	}


}

