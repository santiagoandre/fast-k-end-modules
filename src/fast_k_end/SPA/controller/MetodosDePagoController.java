package fast_k_end.SPA.controller;



import fast_k_end.SPA.model.App;
import fast_k_end.SPA.view.FormCredencialesPago;
import fast_k_end.SPA.view.VMetodosDePago;
import fast_k_end.poo_patterns.mvc_controllers.ItemChangeController;
import fast_k_end.poo_patterns.observer.Observed;


public class MetodosDePagoController extends ItemChangeController

{

	private VMetodosDePago view;
	private App app;
	
	public MetodosDePagoController(VMetodosDePago aView, Observed aModel) {
		super();
        view = aView;
        app = (App) aModel;

	}
	@Override
	public void actualizar() {
		
		String metodoPago = view.getMetodoPago();
		app.seleccionarMetodoPago(metodoPago);
		
		FormCredencialesPago from = view.getFormCredenciales();
		from.actualizar(app, metodoPago);
		
	}

	@Override
	public boolean isValid() {
		return !view.getMetodoPago().isEmpty();
	}
}

