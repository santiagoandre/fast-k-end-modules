package fast_k_end.SPA.controller;

import fast_k_end.SPA.model.AbsApp;
import fast_k_end.SPA.view.VMetodosDePago;
import fast_k_end.poo_patterns.mvc_controllers.WindowsController;
public class CerrarSistemaController extends WindowsController {
	private AbsApp app;
	private VMetodosDePago view;

	public CerrarSistemaController(AbsApp app, VMetodosDePago view){
		this.app = app;
		this.view = view;
	}
	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void actualizar() {
		app.abortar();
		view.setVisible(false);
	}

}
