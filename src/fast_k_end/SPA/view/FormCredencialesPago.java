package fast_k_end.SPA.view;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fast_k_end.SPA.model.App;

public class FormCredencialesPago extends JPanel {
	private JButton btnPagar;
	private VMetodosDePago padre;
	private ArrayList<JTextField> textboxs;
	public FormCredencialesPago(VMetodosDePago parent){
		padre = parent;
		btnPagar = new JButton("Pagar");
		textboxs  = new  ArrayList<JTextField>();
	}
	public VMetodosDePago getPadre(){
		return padre;
	}
	public void actualizar(App app,String metodoPago) {//este metodo es como el de la vista
		// pero este metodo siempre es llamado por la vista principal asi no extendi de view
		
		
		//limpiar panel
		this.removeAll();
		textboxs.clear();
		//se carga el formulario correspondiente
		HashMap<String, Class> credenciales  = app.getCredencialesNecesarias();
		GridLayout layout = new GridLayout(3,2);
		this.setLayout(layout);
		for (Entry<String, Class> entry : credenciales.entrySet()) {
			addCampo(entry.getKey(),entry.getValue());
		}
 
		//se aniade de nuevo el boton pagar
		this.add(btnPagar);
		repaint();
	}

	private void addCampo(String nombre, Class tipo) {
		
		JLabel label = new JLabel(nombre,JLabel.CENTER);
		this.add(label);
		JTextField textField = new JTextField(10);
		this.add(textField);
		textboxs.add(textField);
		//falta lo del tipo, eso va en un controller.
	}
	
	public void setController(ActionListener c){

        btnPagar.addActionListener(c);
	}
	public boolean esValido(){
		//el formulario se lleno?
		for(JTextField tbx:textboxs){
			if(tbx.getText().isEmpty())
				return false;
		}
		return true;
	}
	public ArrayList<String> getCredenciales() {
		ArrayList<String> credenciales = new ArrayList<>();
		for(JTextField tbx:textboxs){
			credenciales.add(tbx.getText());
		}
		return credenciales;
		
	}

	
	
	

}
