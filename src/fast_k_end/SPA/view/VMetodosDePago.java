package fast_k_end.SPA.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ItemListener;
import java.awt.event.WindowListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;

import fast_k_end.SPA.controller.CredencialesPagoController;
import fast_k_end.SPA.controller.MetodosDePagoController;
import fast_k_end.SPA.model.App;
import fast_k_end.poo_patterns.observer.*;



public class VMetodosDePago extends JFrame implements Observer{

	private JPanel contentPane;
	private FormCredencialesPago FromCredenciales;
	private JComboBox cbxMetodosDePago;
	public VMetodosDePago() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		cbxMetodosDePago = new JComboBox();
		contentPane.add(cbxMetodosDePago, BorderLayout.NORTH);
		FromCredenciales = new FormCredencialesPago(this);
		contentPane.add(FromCredenciales, BorderLayout.SOUTH);

	
	}
   

	@Override
	public void notify(Observed aModel, ObserverCode code) {
		showMensaje(code);
		//se cargan los metodos de pago en el combobox
				App app = (App) aModel;
				if(code.equals(ObserverCode.PAYSUCCESS))
					this.setVisible(false);
				else if(!this.isVisible())
					this.setVisible(true);			
				Object[] metodos =  app.getTiposMetodosDePago().toArray();
				DefaultComboBoxModel  cbxModel = new DefaultComboBoxModel (metodos);
				this.cbxMetodosDePago.setModel(cbxModel);
		
	}

	public void showMensaje(ObserverCode code) {
		String msg;
		if(code.equals(ObserverCode.PAYSUCCESS))
				msg ="Pago exitoso";
				else if(code.equals(ObserverCode.UNAUTHENTICATED))
						msg ="Credenciales invalidas";
				else if(code.equals(ObserverCode.PAYERROR))
						msg ="Ah ocurrido un error en el sistema, porfavor contacte al adiministrador";
				else if(code.equals(ObserverCode.LAUNCH))
					msg = "Alzando sistema";
				else
					msg = "Observer code: " + code;
			System.out.println(msg);
		
	}
	public void setControllerCbx(ItemListener c){
		this.cbxMetodosDePago.addItemListener( c);
	}

	public void setControllerWindow(WindowListener c){
		this.addWindowListener(c);
	}

	public String getMetodoPago() {
		return this.cbxMetodosDePago.getSelectedItem().toString();
	}

	public FormCredencialesPago getFormCredenciales() {
		return this.FromCredenciales;
	}



	
}
