package fast_k_end.persistence_system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fast_k_end.file_system.File;
/**
 * Obtiene informacion guardada en archivos planos.
 *
 */
// pueden ser mas, dependiendo de quienes usen esta clase.
public  class FilePersistence implements IPersistence{
	private String basePath = "persistence";
	private String extencion = ".txt";	
	private String separator = "/";
	private HashMap<String,File> files = new HashMap<>();//name, file

	private void loadBase(String name){
		if(files.containsKey(name))
			return;
		String pathFile = basePath + separator + name + extencion;
		File file = new File();
		file.abrirArchivo(pathFile);
		files.put(name, file);
				
	}
	@Override
	public List<String[]> getData(String entityname){
		ArrayList<String[]> data = new ArrayList<>(); 
		loadBase(entityname);
		File file = files.get(entityname);
		String sig = file.Leer();
		while(sig!= null && !sig.isEmpty()){//se recorren todos los metodos de pago reistrados en el archivo, liena a linea
			data.add(sig.split(separator));			
			sig = file.Leer();
		}
		return data;

	}
	@Override
	public String[] getDataOfTarget(String nameFile,String[] keysTarget){
		loadBase(nameFile);
		File file = files.get(nameFile);
		String sig = file.Leer();
		while(sig!= null && !sig.isEmpty()){//se recorren todos los metodos de pago reistrados en el archivo, liena a linea
			String [] data =  isDataOfTarget(sig,keysTarget);
			if(data != null){
				file.close();
				return data;
			}			
			sig = file.Leer();
		}
		file.close();
		//files.remove(nameFile,file);
		return null;
	}
	private String[] isDataOfTarget(String dataString, String[] keysTarget) {
		String[] data = dataString.split(separator);
		int  i = 0;
		for(String key : keysTarget){
			if(!data[i].equals(key))
				return null;
			i++;
		}
		return data;
	}
	
	

}
