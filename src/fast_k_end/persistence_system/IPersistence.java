package fast_k_end.persistence_system;

import java.util.List;
/**
 * Es una interfaz para obtener informacion de una parsistencia, ya sea una base de datos, archivos etc.
 *
 */
public interface IPersistence {
	/**
	 * Obtiene toda la informacion guardada en una entidad, ej: un archivo, una tabla en una base de datos.
	 */
	public List<String[]> getData(String entityname);
	/**
	 * Obtiene registros que cumplen una condicion equals aplicada a algunas de sus columnas de una entidad
	 */
	public String[] getDataOfTarget(String nameFile,String[] keysTarget);

}
