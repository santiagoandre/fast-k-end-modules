/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fast_k_end.poo_patterns.mvc_controllers;

import java.awt.event.ItemEvent;

import java.awt.event.ItemListener;
/**
 *
 * @author DD
 */
public abstract  class ItemChangeController implements ItemListener,IController{

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(isValid())
            actualizar();
    }

  
}
