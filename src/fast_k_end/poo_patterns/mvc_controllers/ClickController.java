package fast_k_end.poo_patterns.mvc_controllers;



import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public abstract  class ClickController  implements MouseListener,IController{

    
    @Override
    public void mouseClicked(MouseEvent me) {
      if(isValid())
          actualizar();
    }

    @Override
    public void mousePressed(MouseEvent me) {
        
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }




}

