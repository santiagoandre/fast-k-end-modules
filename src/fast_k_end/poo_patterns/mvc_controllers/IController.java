package fast_k_end.poo_patterns.mvc_controllers;

public interface IController {
	  /**
	   * Determina si es valida la informacion capturada por la vista
	   * @return
	   */
	  public  boolean isValid() ;  
	  /**
	   * Este metodo recoge la informacion capturada por la vista y se la manda al modelo para que este cambie
	   */
	  public  void actualizar();
}
