package fast_k_end.poo_patterns.mvc_controllers;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public abstract  class ButtonController  implements ActionListener,IController{

    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(isValid())
          actualizar();
    
    }
    
}

