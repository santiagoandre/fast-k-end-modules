/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fast_k_end.DataAccess;

import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author Santiafo
 */
public class TestClassUser implements Serializable{
    private static int MAXNAME = 30;
    private static int MAXID = 10;
    private static int MAXUSER = 10;
    private static int MAXPASSWORD = 10;
    
    private String fullname;
    private int id;
    private String user;
    private String  password;
    
    public TestClassUser(String fullname, int id, String user, String password) {
        this.fullname = fullname;
        this.id = id;
        this.user = user;
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }
/*
    public void setFullname(String fullname) {
        int space = MAXNAME-  fullname.length();
        if(space<0){
        this.fullname = fullname.substring(MAXNAME);
        }else if (space ==0){
            this.fullname = fullname;
        }else{
           String spaces = new String(new char[space]);
           this.fullname = fullname+spaces; 
        }
    }
*/

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "fullname=" + fullname + ", id=" + id + ", user=" + user + ", password=" + password + '}';
    }
    
}
