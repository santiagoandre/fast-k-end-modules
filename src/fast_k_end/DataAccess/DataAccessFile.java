package fast_k_end.DataAccess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.function.Predicate;

public class DataAccessFile implements IDataAccess {

    private static final String EXT = ".txt";
    private static final String mode = "rws";
    private static final String METADATA = ".METADATA";
    private static final String SPLIT = "/";
    private final String basename;

    public DataAccessFile(String basename) {
        this.basename = basename;
    }

    private RandomAccessFile open(String entityname) {
        RandomAccessFile randomaccess = null;
        try {
            randomaccess = new RandomAccessFile(basename + SPLIT + entityname + EXT, mode);
        } catch (FileNotFoundException ex) {
            System.out.print("Hola");
        }

        return randomaccess;
    }

    @Override
    public boolean insertInto(String entity, Object obj) {
        RandomAccessFile file = open(entity);
        boolean result = false;
        try {
            byte[] data = Serializer.serialize((Serializable) obj);
            int sizerecord = this.getSizeRecords(entity);
            if (sizerecord == -1) {
                this.addSizeRecords(entity, data.length + 1);
                sizerecord = data.length + 1;
                System.out.println("Update sizerecords");
            }
            if (sizerecord == data.length + 1) {

                if (file.length() != 0) {
                    file.seek(file.length() + 1);
                }
                file.write(data);
                result = true;
                System.out.println("Store object: " + obj + "into " + entity);
            }
            file.close();
        } catch (IOException ex) {
        }
//        updateIf("asdas",(Object o)->true,(Object o)-> o);

        return result;
    }

    @Override
    public boolean updateIf(String entity, Predicate p, IFUpdate updater) {
        boolean result = false;
        Object obj;
        RandomAccessFile file = open(entity);
        int sizerecord = getSizeRecords(entity);
        if (sizerecord == -1) {
            return false;
        }
        byte[] data = new byte[sizerecord];
        try {

            file.read(data);
            obj = Serializer.deserialize(data);
            while (file.getFilePointer() < file.length()) {
                if (p.test(obj)) {
                    // System.out.println("antes: "+obj);
                    updater.update(obj);
                    // System.out.println("Actuualizado: "+obj);
                    //System.out.println(((double)file.getFilePointer())-sizerecord+" = "+(((double)file.getFilePointer())/sizerecord-1)*sizerecord);
                    // System.out.println(i+" = "+(((double)file.getFilePointer())/sizerecord-1));
                    file.seek(file.getFilePointer() - sizerecord);
                    file.write(Serializer.serialize((Serializable) obj));
                    result = true;
                }
                file.read(data);
                obj = Serializer.deserialize(data);

            }
            file.close();
        } catch (IOException | ClassNotFoundException ex) {
        }

        return result;
    }

    @Override
    public Object getIf(String entity, Predicate p) {
        Object obj = null;
        RandomAccessFile file = open(entity);
        int sizerecord = getSizeRecords(entity);
        if (sizerecord == -1) {
            return false;
        }
        byte[] data = new byte[sizerecord];
        try {

            file.read(data);
            obj = Serializer.deserialize(data);
            while (file.getFilePointer() < file.length()) {
                if (p.test(obj)) {
                    break;
                }
                file.read(data);
                obj = Serializer.deserialize(data);
            }
            if (file.getFilePointer() >= file.length()) {
                obj = null;
            }
            file.close();

        } catch (IOException | ClassNotFoundException ex) {
        }

        return obj;
    }

    @Override
    public Iterable<Object> selectIf(String entity, Predicate p) {
        ArrayList<Object> objects = new ArrayList<>();
        RandomAccessFile file = open(entity);
        int sizerecord = getSizeRecords(entity);
        if (sizerecord == -1) {
            return null;
        }
        byte[] data = new byte[sizerecord];
        try {

            file.read(data);
            Object obj = Serializer.deserialize(data);
            while (file.getFilePointer() < file.length()) {
                if (p.test(obj)) {
                    objects.add(obj);

                }
                file.read(data);
                obj = Serializer.deserialize(data);
            }

            file.close();
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }

        return objects;
    }

    @Override
    public Iterable<Object> selectAll(String entity) {
        ArrayList<Object> objects = new ArrayList<>();
        RandomAccessFile file = open(entity);
        int sizerecord = getSizeRecords(entity);
        if (sizerecord == -1) {
            return null;
        }
        byte[] data = new byte[sizerecord];

        try {
            //file.seek(0);
            file.read(data);
            Object obj = Serializer.deserialize(data);
            while (file.getFilePointer() < file.length()) {

                objects.add(obj);

                file.read(data);
                obj = Serializer.deserialize(data);
            }

            file.close();
        } catch (IOException | ClassNotFoundException ex) {
        }

        return objects;
    }

    private int getSizeRecords(String entityname) {
        int result = -1;
        try {

            BufferedReader bw = new BufferedReader(new FileReader(basename + SPLIT + METADATA + EXT));
            String line = bw.readLine();

            while (line != null) {
                String[] split = line.split(SPLIT);
                if (split[0].equals(entityname)) {
                    result = Integer.parseInt(split[1]);
                    break;
                }
                line = bw.readLine();
            }
            bw.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {
        }
        return result;
    }

    private boolean addSizeRecords(String entityname, int newsize) {
        boolean result = false;
        try {

            BufferedWriter bw = new BufferedWriter(new FileWriter(basename + SPLIT + METADATA + EXT, true));
            bw.write(entityname + SPLIT + newsize);
            bw.newLine();

            bw.close();
            result = true;
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {
        }
        return result;
    }

}
