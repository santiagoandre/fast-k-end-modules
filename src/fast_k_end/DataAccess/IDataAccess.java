/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fast_k_end.DataAccess;

import java.util.function.Predicate;

/**
 *
 * @author Santiafo
 */
public interface IDataAccess {

    public boolean insertInto(String entity, Object obj);

    public boolean updateIf(String entity, Predicate p, IFUpdate updater);

    public Object getIf(String entity, Predicate p);

    public Iterable<Object> selectIf(String entity, Predicate p);

    public Iterable<Object> selectAll(String entity);

}

@FunctionalInterface
interface IFUpdate {

    public void update(Object o);
}
