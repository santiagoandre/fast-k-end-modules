/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fast_k_end.DataAccess;

import java.io.IOException;

/**
 *
 * @author ingesis
 */
public class TestDataAccessFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        DataAccessFile a = new DataAccessFile("store");
        String entity = "users";
        write(a,entity,10,50);
        
        testUpdateIf(entity,a);
        testgetIf(entity,a);
        testSelectAll(entity,a);
        
    }

    public static void write(DataAccessFile a,String entity, int init, int end) {
        
        System.out.print("Test insertinto: " );
        for (int i = init; i <= end; i++) {

            TestClassUser u = new TestClassUser("name" + i, 32, "user", "password");
            a.insertInto(entity, u);
        }
    }

    public static void testSelectIf(String entity,DataAccessFile a) {
        System.out.print("Test selected if: " );
        int i = 0;
        for (Object rg : a.selectIf(entity, (Object us) -> ((TestClassUser) us).getFullname().equals("name123"))) {
            System.out.println(rg);
            i++;
        }
        System.out.print("sert: " +i);

    }

    public static void testSelectAll(String entity,DataAccessFile a) {
        int i = 0;
        System.out.print("Test inselect all: " );
        for (Object rg : a.selectAll(entity)) {
            System.out.println(rg);
            i++;
        }
        System.out.print("Num registros: " + i);

    }

    public static void testgetIf(String entity,DataAccessFile a) {
        System.out.print("Test getIf: " );
        TestClassUser u = (TestClassUser) a.getIf(entity, (Object us) -> ((TestClassUser) us).getFullname().equals("name20"));
        System.out.println(u);
    }

    public static void testUpdateIf(String entity,DataAccessFile a) {
        System.out.print("Test updateif: " );
        boolean result = a.updateIf(entity, (Object us) -> ((TestClassUser) us).getFullname().equals("name25"), (Object us) -> {
            ((TestClassUser) us).setFullname("nuevoo");
            ((TestClassUser) us).setId(123123123);
        });
        System.out.print(result);

    }

}
